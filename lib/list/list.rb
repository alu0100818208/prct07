module List
    
    Node = Struct.new(:value, :next)
    
    class List

        attr_reader :ini
        def initialize(nodo)
            @ini = nodo
        end
        
        def extraer_ini()
            if (@ini != nil)
                dummy = @ini
                @ini = @ini.next
                return dummy
            else
                return nil
            end
        end
        
        def insertar_elemento(nodo)
            if (@ini == nil)
                @ini = nodo
            else
                aux = @ini
                @ini = nodo
                @ini.next = aux
                
            end
        end
        
        def insertar_varios(vector)
            vector.each do |i|
                insertar_elemento(i)
            end
        end
    end



end