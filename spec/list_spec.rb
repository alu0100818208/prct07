require 'spec_helper'
require 'list'

describe List do
  nodo = List::Node.new(4, nil)
  listita = List::List.new(nodo)
  nodo2 = List::Node.new(7, nil)
  vectornodos = []
  num = 10
  
  for i in 0..2 do
    vectornodos[i] = List::Node.new(num, nil)
    num+=1
  end

  it "El nodo se crea correctamente" do
    expect(nodo.value).to eq(4)
    expect(nodo.next).to eq(nil)
  end

  it "Se inicializa la clase." do
    expect(listita.ini).to eq(nodo)
  end
  
  it "Se extrae la cabeza." do
    expect(listita.extraer_ini()).to eq(nodo)
  end
  
  it "Insertamos un elemento" do
    listita.insertar_elemento(nodo2)
    expect(listita.ini).to eq(nodo2)
  end
  
  it "Insertamos varios elementos" do
    listita.insertar_varios(vectornodos)
    expect(listita.ini).to eq(vectornodos[2])
  end


end
